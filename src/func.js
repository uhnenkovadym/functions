const getSum = (str1, str2) => {
  var regExp = /[a-zA-Z]/g;
  if (typeof str1 === "object" || typeof str2 === "object" || typeof str1 === "number" || typeof str2 === "number"){
    return false;}
  if(regExp.test(str1) || regExp.test(str2)){ 
    return false;}
  if(str1 == "" || str2 == ""){
    return str1 == "" && str2 == "" ? '0' : str1 == "" ? str2 : str1;}
  else{
    return String(+str1 + +str2);}
  };

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  let posts = 0;
  let comment = 0;
for(let i = 0; i<listOfPosts.length; i++)
{
    if(listOfPosts[i].author == authorName)
    {
        posts++;
    }
    if(listOfPosts[i].comments)
    {
        for(let j = 0; j<listOfPosts[i].comments.length; j++)
      {
           if(listOfPosts[i].comments[j].author == authorName)
           {
               comment++;
           }
      }
    }
}
 return `Post:${posts},comments:${comment}`;
};

const tickets=(people)=> {
  const filmCost = 25;
  let totalHash = 0;
  const hash = new Map();
  hash.set(100, 0);
  hash.set(50, 0);
  hash.set(25, 0);

  for (const money of people) {
    if (money === 25) {
      totalHash += 25;
      hash[25] += 1;
      continue;
    }

    let diff = money - filmCost;
    if (diff > totalHash) return "NO";
    totalHash -= diff;

    for (const k of hash.keys()) {
        if (diff > k && hash[k] > 0) {
            const count = Math.max(Math.ceil(diff / hash[k]), hash[k]);
            hash[k] -= count;
            diff -= count * k;
        }
    }

    hash[money] += 1;
    totalHash += money;
  }

  return "YES";
};


module.exports = {getSum, getQuantityPostsByAuthor, tickets};
